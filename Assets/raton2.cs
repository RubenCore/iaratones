﻿using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using UnityEngine;

public class raton2 : Agent {

    private Vector3 posInicial;
    public delegate void ResetDelegate();
    public event ResetDelegate OnReset;
    public BoxCollider2D[] checkpoints;
    public CircleCollider2D[] quesos;
    public int vel = 10;
    private Rigidbody2D ratoncio;

    public override void Initialize()
    {        
        posInicial = this.transform.localPosition;
        ratoncio = this.GetComponent<Rigidbody2D>();
        
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        float horizontal = actions.DiscreteActions[0]-1;
        float vertical = actions.DiscreteActions[1]-1;
        Debug.Log(vertical + " " + horizontal);
        ratoncio.velocity = new Vector2(horizontal,vertical)*Time.deltaTime*vel;
       
    }

    public override void OnEpisodeBegin()
    {
        AddReward(-5.5f);
        this.transform.localPosition = posInicial;
        foreach (BoxCollider2D b in checkpoints)
        {
            b.enabled = true;
        }
        foreach (CircleCollider2D a in quesos)
        {
            a.enabled = true;
        }
        OnReset?.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "checkpoint")
        {
            AddReward(0.2f);
            collision.enabled = false;
            Debug.Log(GetCumulativeReward());
        }
        if (collision.transform.tag == "queso")
        {
            AddReward(0.55f);
            collision.enabled = false;
            Debug.Log(GetCumulativeReward());
        }
        if (collision.transform.tag == "quesoFinal")
        {
            AddReward(3f);
            collision.enabled = false;
            Debug.Log(GetCumulativeReward());
            EndEpisode();
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "laberinto")
        {
            AddReward(-5.5f);
            Debug.Log(GetCumulativeReward());
            EndEpisode();
        }
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<int> a = actionsOut.DiscreteActions;
        a[0] = (int)Input.GetAxisRaw("Horizontal")+1;
        a[1] = (int)Input.GetAxisRaw("Vertical")+1;
    }

}
